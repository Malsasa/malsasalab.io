---
layout: page
title: Proprietary Software?
permalink: /proprietary-bahaya/
---

Paper-paper yang saya tulis dan publikasikan di dalam blog ini membahas proprietary software (perangkat lunak tidak bebas) dalam rangka menjauhkan masyarakat darinya. Pendekatan yang saya lakukan adalah dengan memberikan bukti-bukti bahwa p. software memutus hubungan sosial dan merugikan masyarakat. Contoh p. software yang banyak disinggung dalam paper saya adalah Microsoft Windows, Microsoft Office (Word, Excel, PowerPoint), IDM, MATLAB, dan AutoCAD yaitu yang paling umum dimanfaatkan di masyarakat terutama di sektor pendidikan. Saya ingin menyediakan paper yang membuktikan dengan jelas bahwa p. software adalah ketidakadilan dan tidak menguntungkan masyarakat sebab mereka mengambil wewenang yang bukan haknya pada komputer yang bukan komputernya terhadap pengguna. Saya berharap paper-paper saya dijadikan rujukan terutama di kalangan akademia dan pembuat keputusan untuk mengenali bahaya p. software sehingga tercapai keputusan untuk menghilangkannya secara keseluruhan dari masyarakat. Saya mendedikasikan paper-paper saya untuk kemaslahatan yang adil bagi seluruh masyarakat termasuk para sumber p. software itu sendiri.

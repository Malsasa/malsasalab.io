---
layout: page
title: Migrasi
---
Migrasi ke free software dari proprietary software sering disalahpahami sebagai sebatas _masalah pilihan_ atau sebatas _masalah legalitas_ dan bukannya *masalah sosial*. Kesalahpahaman ini perlu diluruskan agar setiap orang yang bermigrasi mengerti masalah yang dihadapinya dan dapat menyelesaikan masalah itu. 

# Bukan 'Masalah Pilihan'

Migrasi dari p. software ke f. software bukanlah semata 'masalah pilihan' seperti memilih apel daripada jeruk. Pemahaman bahwa migrasi ini hanya masalah pilihan tidak dapat menyelesaikan masalah apa pun dan pada faktanya justru menciptakan masalah-masalah baru. Pemahaman demikian tidak dapat menyembuhkan masyarakat dari masalah sesungguhnya yaitu p. software dan justru pada waktunya akan menarik kembali masyarakat yang telah bermigrasi ke p. software tersebut. 

# Bukan 'Masalah Legalitas'

Sama dengan itu, migrasi ini juga bukan semata 'masalah legalitas' walaupun alasan legalitas adalah legitimate bagi banyak orang. Namun pemahaman demikian juga tidak menyelesaikan masalah dan pada faktanya ikut menciptakan masalah baru. Pemahaman ini tidak dapat memberi jawaban yang jelas bahwa ilegalitas (pelanggaran hukum) yang dilakukan oleh masyarakat itu disebabkan satu-satunya karena p. software dan tidak menerangkan bahwa kesalahan tidak berada pada masyarakat. 

# Ini Masalah Sosial

Masalah migrasi ini adalah karena masyarakat perlu menyelesaikan masalah sosial yang bernama p. software. Masalah sosialnya ialah bahwa hak sosial (gotong-royong) dan wewenang atas software yang seharusnya diterima oleh tiap-tiap pengguna tidak diterima, yang menjadikan tiap-tiap pengguna tersebut di bawah wewenang mutlak pengembang p. software secara massal. P. software mengganti moral masyarakat sosial dari menghormati gotong-royong menjadi melarang dan mengkriminalkannya secara massal di antaranya dengan slogan "pembajakan" yang tidak benar. P. software sekali masuk ke masyarakat maka cenderung menarik orang kepada sekian banyak p. software lain dan mengekangnya agar tidak bisa keluar darinya. Oleh karena itu migrasi ke f. software adalah karena masalah sosial bernama p. software dalam rangka menyelesaikannya dan menghilangkannya.

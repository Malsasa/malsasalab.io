---
title: Kamus Istilah
layout: page
permalink: /glosarium/
---
* Ini adalah table of content
{:toc}

# Software
Soft: lunak, sifat non-tangible, sifat non-fisik; ware: perangkat; software: perangkat lunak, berupa seperti resep masakan dengan rumus-rumus matematika berbahasa Inggris yang memiliki kegunaan tertentu yang dibuat untuk dioperasikan pada komputer.

# Komputer
Dari kata __computer__, berarti mesin hitung. Tugas komputer adalah menghitung software dan data. Perhitungan yang dilakukan oleh komputer dinamakan pengoperasian (terhadap software) atau pengolahan (terhadap data).

# Source code
Source: sumber, code: kode; source code: kode sumber, bentuk asli software yang ditulis oleh programernya, bentuk software sebelum diterjemahkan menjadi binary code. 

# Binary code
Binary: biner, nol dan satu; code: kode; binary code: kode biner, bentuk sekunder software setelah diterjemahkan agar dapat dioperasikan pada tipe komputer tertentu yang tidak dapat dioperasikan di tipe komputer lain. 

# Free software
Free: bebas, merdeka; free software: perangkat lunak yang penggunanya bebas terhadapnya. Pengguna memiliki wewenang penuh terhadap software. Definisi free software sebagaimana diterbitkan oleh organisasi [Free Software Foundation](http://www.gnu.org/philosophy/free-sw.html).

# Compiling
Dari kata _to compile_, _compiler_, _compiled_; compiling: penerjemahan dari source code ke binary code, proses membuat versi software dalam bentuk yang bisa langsung dioperasikan di satu tipe komputer tertentu.

# Libre software
Sinonim _free software_. 

# Proprietary software
Proprietary: bersifat tetap termiliki, dimiliki oleh pihak tertentu; proprietary software: perangkat lunak yang tetap dimiliki oleh pihak lain walaupun sudah dipegang oleh pengguna. Pengguna tidak memiliki wewenang penuh terhadap software.

# Nonfree software
Sinonim _proprietary software_. Nonfree: tidak bebas, tidak merdeka; nonfree software: perangkat lunak yang penggunanya tidak bebas terhadapnya. 

# Sistem operasi
Satu kesatuan dari beberapa software yang terintegrasi yang tanpanya komputer tidak bisa beroperasi. Sistem operasi adalah satu lapisan software yang terus menerus beroperasi selama komputer menyala yang di atasnya berbagai software aplikasi dioperasikan.

---
layout: komentar
title: Komentar
permalink: /komentar/
---
Sampaikan komentar-komentar Anda atas paper-paper di kolom komentar berikut. Setiap komentar dimoderasi.

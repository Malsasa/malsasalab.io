---
layout: page
title: Sejarah Ringkas Ubuntu GNU/Linux Sejak 2004 Sampai 2018
---
<img src="/paper/sejarah-ubuntu.jpeg" width="300px"><br>
Abstrak: Ubuntu adalah sistem operasi GNU/Linux yang dikenal mudah digunakan dan banyak jumlah penggunanya di masyarakat. Sejarah Ubuntu secara ringkas dimulai pada tahun 2004 dengan penerbitan pertamanya oleh Canonical Ltd., mengalami 28 kali rilis versi dan 7 di antaranya LTS sampai versi 18.04 di 2018, memiliki 28 nama hewan sebagai kode nama masing-masing, pernah mengalami pergantian office suite dan desktop environment, pernah mengalami inkonsistensi rilis, dan pernah memiliki layanan masyarakat yang mendunia seperti ShipIt dan Brainstorm. Paper ini akan membahasnya secara ringkas untuk versi desktop saja dengan data-data dan referensinya dan pada akhirnya akan merangkum sejarah itu dalam butir-butir singkat.
Kata kunci: ubuntu, gnu/linux, turunan debian, sejarah, sistem operasi, distribusi perangkat lunak bebas

Lisensi paper: [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)

Tanggal rilis: 25 April 2018

Unduh paper:
* [PDF](/paper/sejarah-ubuntu.pdf)
* [ODT](/paper/sejarah-ubuntu.odt)

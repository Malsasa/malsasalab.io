---
layout: page
title: Berita
permalink: /berita/
---
Halaman Berita ini memublikasikan catatan perubahan terbaru mengenai blog ini dan paper yang saya publikasikan.

25 April 2018: publikasi paper perdana [Sejarah Ubuntu](/2018/04/25/sejarah-ubuntu.html) pada blog ini berhasil saya lakukan. Pembersihan blog belum sempat saya lakukan seperti menghilangkan posting percobaan dan lain-lain. Rekonstruksi tampilan blog belum saya pikirkan. Pengerjaan paper selanjutnya sedang dalam proses. 

---
layout: page
title: Tentang
permalink: /tentang/
---

Blog ini memublikasikan paper-paper yang saya tulis mengenai GNU dan sejarahnya, free software dan komunitasnya, dalam bahasa Indonesia. Saya tidak berafiliasi dengan institusi pendidikan maupun organisasi mana pun dalam penyusunan paper-paper saya.

Mengapa saya menulis? Karena saya memandang tulisan mengenai GNU, free software, komunitas GNU/Linux sangat diperlukan oleh masyarakat. Pengetahuan mengenai free software tidak ada di sekolah-sekolah dan kampus-kampus walaupun sebagian dari mereka mengajarkan GNU/Linux. Saya ingin masyarakat beralih dari proprietary software ke free software, menolak proprietary software, dan untuk itu masyarakat berhak tahu apa yang salah darinya dan apa solusinya. Paper-paper yang saya tulis membahas itu.

Di antara paper yang sedang saya tulis:
* Sejarah Ubuntu GNU/Linux Sejak 2004 Sampai 2018 ([telah terbit](/2018/04/25/sejarah-ubuntu.html))
* Mengapa Proprietary Software Merupakan Masalah Sosial yang Perlu Diselesaikan
* Perbedaan Perangkat Lunak Berbayar dan Proprietary

Di antara blog-blog yang saya miliki dan yang saya menulis di sana:
* [malsasa.wordpress.com](https://malsasa.wordpress.com) - blog utama
* [restava.wordpress.com](https://restava.wordpress.com) - jurnal aktivitas free software pribadi
* [desaininkscape.wordpress.com](https://desaininkscape.wordpress.com) - desain grafis dengan Scribus dan Inkscape
* [kursusteknoplasma.wordpress.com](https://kursusteknoplasma.wordpress.com) - kursus GNU/Linux
* [bermigrasi.wordpress.com](https://bermigrasi.wordpress.com) - kumpulan informasi migrasi ke free software
* modemlinux.wordpress.com - kumpulan panduan instalasi modem di GNU/Linux
* offlineuser.wordpress.com - kumpulan panduan instal-menginstal program secara offline di GNU/Linux
* hardwareubuntu.wordpress.com - kumpulan perangkat keras yang kompatibel GNU/Linux
* linuxdreambox.wordpress.com - jurnal aktivitas free software pribadi dalam bahasa Inggris
* blog.cilsy.id (dulunya www.linuxku.com, sebagai penulis tamu)
* www.ubuntubuzz.com (sebagai penulis tamu)

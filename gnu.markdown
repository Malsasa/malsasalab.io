---
layout: page
title: Apa Itu GNU?
---
GNU adalah sistem operasi yang oleh orang-orang disebut "Linux". Bila Anda mendengar nama "Linux", saksikan, umumnya yang dimaksudkan adalah GNU. Sistem operasi komputer yang dinamakan Ubuntu, Fedora, openSUSE, adalah turunan-turunan GNU, lumrahnya disebut GNU, hanya saja pada praktiknya orang sebut "Linux". GNU umumnya dipasangkan dengan kernel bernama Linux dan kombinasinya disebut GNU/Linux (atau GNU+Linux).

GNU adalah sistem operasi bebas yang pertama di dunia. Sebelum GNU ada, semua sistem operasi adalah tidak bebas. GNU dibuat murni untuk tujuan membebaskan pengguna komputer dari perangkat lunak tidak bebas. Bebas yang dimaksud adalah pengguna software bebas dari kuasa pengembang. Bebas yang dimaksud adalah pengguna bebas mempergunakan, mempelajari, mengubah, mendistribusikan software (dalam bentuk source code maupun selainnya) dan termasuk memperjualbelikannya. Bila Anda mempergunakan sistem operasi bebas dan mendengar bahwa itu "Linux", sebenarnya itu GNU.

GNU memiliki 3 lisensi untuk komponen-komponennya yang semuanya adalah lisensi bebas yang copyleft. Lisensi itu adalah GNU General Public License, GNU Lesser General Public License, dan GNU Affero General Public License. Ketiga lisensi software ini adalah bebas dan menjamin kebebasan itu di sisi pengguna tidak bisa dicabut oleh siapa pun.
